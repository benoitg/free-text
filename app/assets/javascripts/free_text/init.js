autoActivatedTab = GENTICS.Aloha.i18n(GENTICS.Aloha, 'floatingmenu.tab.format')

var saveButton = new GENTICS.Aloha.ui.Button({
    "label" : "Enregistrer les modifications de la page",
    "icon" : "/assets/free_text/aloha/images/save.png",
    "onclick" : function(element, event) {
        var editables = [];
        $.each(GENTICS.Aloha.editables, function() {
            if(this.isModified())
                editables.push({
                    key: this.obj.attr("data-key"),
                    locale: this.obj.attr("data-locale"),
                    content: this.getContents()
                });
        });
        if (editables.length > 0)
            $.ajax({
                type: "PUT",
                url: "/free_texts",
                data: {
                    "editables": editables
                },
                success: function(){
                    console.debug("success");
                }
            });
        return true;
    },
    "size" : "large"
});
GENTICS.Aloha.FloatingMenu.addButton('GENTICS.Aloha.continuoustext', saveButton, "Enregistrer", 0);

$(document).ready(function() {
    $("div.free_text_aloha").aloha();
});