class FreeText::TextsController < ApplicationController
  def update
    editables = params["editables"]
    editables.each do |e|
      datas = e[1]
      editable_text = FreeText::Text.find_by_key_and_locale(datas["key"], datas["locale"])
      editable_text.update_attribute("content", datas["content"])
    end
    render :json => {:success => false}
  end

  def aloha_config
    render "free_text/aloha_config"
  end
end
