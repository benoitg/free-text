module FreeText::TextsHelper
  def free_text_styles
    FreeText.styles.to_s.html_safe
  end

  def free_text_restrictions
    FreeText.restrictions.to_s.gsub('=>', ': ').html_safe
  end
end