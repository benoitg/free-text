module FreeText::TextHelper
  def free_text_init
    result = ""
    if FreeText.authentication.call(controller)
      result += stylesheet_link_tag "free_text"
      result += javascript_include_tag "free_text"
      result += javascript_include_tag aloha_config_free_texts_path
    end
    result.html_safe
  end

#  key : id of the free text
#  options : class ...
  def free_text(key, options={})
    default_style = options[:default_style]
    free_text = FreeText::Text.find_by_key_and_locale(key, I18n.locale.to_s) ||
      FreeText::Text.create(:key => key, :locale => I18n.locale.to_s, :content => "#{"<#{default_style}>" if default_style}Editable text #{key}#{"</#{default_style}>" if default_style}")
    partial_locals = {:free_text => free_text,
      :div_class => options[:class],
      :class_restriction => options[:class_restriction]}
    render :partial => "free_text/partial", :locals => partial_locals
  end
end
