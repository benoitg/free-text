# Provide a simple gemspec so you can easily use your enginex
# project in your rails apps through git.
Gem::Specification.new do |s|
  s.name = "free-text"
  s.summary = "Insert FreeText summary."
  s.description = "Insert FreeText description."
  s.files = Dir["{app,lib,config}/**/*"] + ["MIT-LICENSE", "Rakefile", "Gemfile", "README.rdoc"]
  s.version = "0.0.1"
  s.authors = ["Cyril Gorrieri", "Benoit Garret"]
  
  s.add_dependency('jquery-rails')
end
