FreeText.setup do |config|
  config.authentication = Proc.new { |controller| controller.admin_signed_in? }
  config.restrictions = {
    ".content" => ['b', 'i', 'u', 'sub', 'sup', 'p', 'h2', 'h3', 'removeFormat'],
    ".title" => ['h1']
  }

  # config.styles = ['b']
end