module FreeText
  module Generators
    class InitializerGenerator < Rails::Generators::Base
      source_root File.expand_path("../templates", __FILE__)
 
      def copy_initializer
        copy_file "free-text.rb", "config/initializers/free-text.rb"
        rake("free_text:install:migrations")
        readme "README"
      end
    end
  end
end