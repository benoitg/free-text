module FreeText

  class Engine < Rails::Engine
    isolate_namespace FreeText
    initializer 'engine.helper' do |app|
      ActionView::Base.send :include, TextHelper
    end
  end

  mattr_accessor :authentication
  @@authentication = Proc.new {|controller| true}

  mattr_accessor :restrictions
  @@restrictions = {}

  mattr_accessor :styles
  @@styles = ['b', 'i', 'u', 'sub', 'sup', 'p','h1', 'h2', 'h3', 'removeFormat']

  mattr_reader :controller_name
  @@controller_name = "free_text/texts"

  def self.setup
    yield self
  end
end